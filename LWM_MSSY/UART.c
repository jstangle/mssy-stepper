

#include <avr/io.h>
#include "makra.h"

void UART_init(uint16_t Baudrate){
	//volatile double test=((8000000/16)/Baudrate)-1;
	int ubrr=((F_CPU/16/Baudrate)-1);
	UBRR1H = (uint8_t)(ubrr>>8);
	UBRR1L = (uint8_t)ubrr;
	UCSR1B = (1<<RXEN1)|(1<<TXEN1);// Povolit prijem/vydej
	UCSR1C = (1<<UCSZ01)|(1<<UCSZ00);// Async, Parity-Disabled, 1 Stop bit, 8 data bits
	UCSR1B |= (1 << RXCIE1); //Povoleni preruseni pri prijmu

}
void UART_SendChar(uint8_t data)
{
	while ( !( UCSR1A & (1<<UDRE0)) )
	;
	UDR1 = data;
}
uint8_t UART_GetChar( void )
{
	while ( !(UCSR1A & (1<<RXC0)) )
	;
	return UDR1;
}
void UART_SendString(char *text)
{
	while (*text != 0x00)
	{
		UART_SendChar(*text);
		text++;
	}
}
