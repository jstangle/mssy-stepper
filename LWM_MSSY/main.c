

#include <avr/io.h>
/*- Includes ---------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "config.h"
#include "hal.h"
#include "phy.h"
#include "sys.h"
#include "nwk.h"
#include "sysTimer.h"
#include "halBoard.h"
#include "halUart.h"
#include "main.h"
/*- Stepper Includes -------------------------------------------------------*/
#include <util/delay.h>
#include <math.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include "UART.h"


/*- Definitions ------------------------------------------------------------*/
#ifdef NWK_ENABLE_SECURITY
#define APP_BUFFER_SIZE     (NWK_MAX_PAYLOAD_SIZE - NWK_SECURITY_MIC_SIZE)
#else
#define APP_BUFFER_SIZE     NWK_MAX_PAYLOAD_SIZE
#endif

/*- Stepper Definitions ----------------------------------------------------*/
#define DEV_TYPE_MOTOR	4
#define CMD_READ		1
#define CMD_WRITE		0

/*- Stepper Variables ------------------------------------------------------*/
const int sec = 2.1; // delay in ms for motor calibration
const int step = 510; // one motor step
// int motor = 0; // calibration value
int position = 0; // loaded from eeprom
int range = 100; // calibrated border for motor

/*- Types ------------------------------------------------------------------*/
typedef enum AppState_t
{
	APP_STATE_INITIAL,
	APP_STATE_IDLE,
} AppState_t;

/*- Stepper Device ---------------------------------------------------------*/
typedef struct Data {
	uint16_t cnt;
	char position[10];
	char payload[10];
	uint8_t pay_len;
} Data;
typedef enum State{
	UNREGISTRED = 0,
	REGISTERED,
	HALF_WAY_REGISTERED,
	PERIODIC,
	ONE_SHOT,
	RADIO_ENABLE_SEND,
	RADIO_DISABLE_SEND
} state;
typedef struct Config{
	state reg_state;
	state mode;
} Config;

static Data data;
static Config config;
char rec_buf[50]; // buffer for received data

/*- Prototypes -------------------------------------------------------------*/
static void appSendData(void);
/*- Stepper Prototypes -----------------------------------------------------*/
static void prepare_payload(Data * data);
static void receive_data_handle(NWK_DataInd_t *ind);
static void resolve_state(SYS_Timer_t *timer);

/*- Variables --------------------------------------------------------------*/
static AppState_t appState = APP_STATE_INITIAL;
static SYS_Timer_t appTimer;
static NWK_DataReq_t appDataReq;
static bool appDataReqBusy = false;
static uint8_t appDataReqBuffer[APP_BUFFER_SIZE];
static uint8_t appUartBuffer[APP_BUFFER_SIZE];
static uint8_t appUartBufferPtr = 0;

/*- Implementations --------------------------------------------------------*/

/*************************************************************************//**
*****************************************************************************/
static void appDataConf(NWK_DataReq_t *req)
{
	printf("[RADIO ] Send SUCCESS\r\n");
	appDataReqBusy = false;
	(void)req;
}

/*************************************************************************//**
*****************************************************************************/
static void appSendData(void)
{
	if (appDataReqBusy || 0 == appUartBufferPtr)
	{
		return;
	}
	
	/* Copy Payload to NWKbuf */
	memcpy(appDataReqBuffer, appUartBuffer, appUartBufferPtr);
	appUartBufferPtr = data.pay_len;

	/* Print out */
	printf("[PAYLOD] %s\r\n", data.payload);

	/* Prepare structure */
	appDataReq.dstAddr = 1-APP_ADDR;
	appDataReq.dstEndpoint = APP_ENDPOINT;
	appDataReq.srcEndpoint = APP_ENDPOINT;
	appDataReq.options = NWK_OPT_ENABLE_SECURITY;
	appDataReq.data = appDataReqBuffer;
	appDataReq.size = appUartBufferPtr;
	appDataReq.confirm = appDataConf;
	NWK_DataReq(&appDataReq);

	appUartBufferPtr = 0;
	appDataReqBusy = true;
}

/*************************************************************************//**
*****************************************************************************/
void HAL_UartBytesReceived(uint16_t bytes)
{
	/*for (uint16_t i = 0; i < bytes; i++)
	{
		uint8_t byte = HAL_UartReadByte();
		
		if (appUartBufferPtr == sizeof(appUartBuffer))
		appSendData();
		
		if (appUartBufferPtr < sizeof(appUartBuffer))
		appUartBuffer[appUartBufferPtr++] = byte;
	}

	SYS_TimerStop(&appTimer);
	SYS_TimerStart(&appTimer);*/
}

/*************************************************************************//**
*****************************************************************************/
static void appTimerHandler(SYS_Timer_t *timer)
{
	/*
	printf("[RADIO ] Send Interrupt\r\n");
	appSendData();
	(void)timer;*/
}

/****************************************************************************
*****************************************************************************/

/* Stepper Logic --------------------------------------------------------*/
void clockwise_step(){
	Aon;
	_delay_ms(sec);
	Doff;
	Bon;
	_delay_ms(sec);
	Aoff;
	Con;
	_delay_ms(sec);
	Boff;
	Don;
	_delay_ms(sec);
	Coff;
	}


void anticlockwise_step(){
	Don;
	_delay_ms(sec);
	Aoff;
	Con;
	_delay_ms(sec);
	Doff;
	Bon;
	_delay_ms(sec);
	Coff;
	Aon;
	_delay_ms(sec);
	Boff;
}


void clockwise(){
	for(int i = 0; i < step; i++){
		clockwise_step();
	}
}

void anticlockwise(){
	for(int i = 0; i < step; i++){
		anticlockwise_step();
	}
}

void down(){
	clockwise();
	position++;
	/*
	while(motor == 1){
	}*/
}

void up(){
	anticlockwise();
	position--;
}

int get_position(){
	uint8_t i = eeprom_read_byte((uint16_t*)0x0016);
	char hex_position[20];
	itoa(i, hex_position, 16);
	
	return hexadecimal_to_decimal(hex_position)
}

int hexadecimal_to_decimal(int x)
{
      int decimal_number, remainder, count = 0;
      while(x > 0)
      {
            remainder = x % 10;
            decimal_number = decimal_number + remainder * pow(16, count);
            x = x / 10;
            count++;
      }
      return decimal_number;
}

void set_position(int command){
	position = get_position();

	if (command > range){
		command == range;
	}
	else {
		if (position > command){
			while(position-1 > command) { 
				up();
			}
		}
		else if(position+1 < command){
			while(position < command) { 
				down();
			}
		} 
	}
	eeprom_write_byte((uint16_t*)0x0016, position);
}

/*	
ISR(INT7_vect){
	
	CLEARBIT(EIMSK,7);
	_delay_ms(500);
	LED0change;
	if(motor == 1)
	{
		motor = 0;
		UART_SendString("*************\r\n");
		eeprom_write_byte((uint16_t*)0x0016, position);
		uint8_t i = eeprom_read_byte((uint16_t*)0x0016);
		char test[20];
		itoa(i, test, 16);
		UART_SendString(test);
		UART_SendString("\r\n");
		UART_SendString(hexadecimal_to_decimal(test));
	}
	else
	{
		motor = 1;
	}
	SETBIT(EIMSK,INT7);
	_delay_ms(1000);
	SETBIT(EIMSK,INT7);
	motor = false
	
}
*/


/* Stepper Data Receive -------------------------------------------------*/
static bool appDataInd(NWK_DataInd_t *ind)
{
	printf("\r\n[RADIO ] Receiving\r\n");
	// for (uint8_t i = 0; i < ind->size; i++)
	//		HAL_UartWriteByte(ind->data[i]);
	receive_data_handle(ind);
	
	return true;
}

static void receive_data_handle(NWK_DataInd_t *ind){
	
	// Descovery_req
	if(ind->data[0] == 32)
	{
		printf("[MOTOR] Registering SUCCESS\r\n");
		config.reg_state = HALF_WAY_REGISTERED;
		resolve_state(NULL);
	}
	
	// Write
	if(ind->data[0] == 4)
	{
		printf("[MOTOR] Config recv\r\n");
		config.reg_state = REGISTERED;
		
		memset(rec_buf, '\0', sizeof(rec_buf));
		memcpy((char*)rec_buf, ind->data, ind->size);
		
		// reading request -> execution
		
		for(uint8_t pointer = 2; pointer < (ind->size + 2); pointer++)
		{
			if(rec_buf[pointer] == DEV_TYPE_MOTOR){
				pointer++;
				
				if(rec_buf[pointer++] == CMD_WRITE){
					// saving new motor position
					if(rec_buf[pointer++] == 0){
						/* uint32_t position value with size (4 bytes) */
						if(rec_buf[pointer++] == 4){						
							uint32_t new_position = ((uint32_t)rec_buf[pointer++]<<24);
							new_position += ((uint32_t)rec_buf[pointer++]<<16);
							new_position += ((uint32_t)rec_buf[pointer++]<<8);
							new_position += rec_buf[pointer++];
							
							set_position(new_position);
						}
						else {
							printf("Unexpected size\r\n");
						}
					} 
					else {
						printf("Unknown deviceID\r\n");
					}
				}
				else {
					// reading
					printf("POSITION reading\r\n");
					// eventually code for position reading
				}
			}
			
		}
		
	}
}


/*************************************************************************//**
*****************************************************************************/
static void appInit(void)
{
	NWK_SetAddr(APP_ADDR); //APP_ADDR 1-65535
	NWK_SetPanId(0x4567); //APP_PANID
	PHY_SetChannel(APP_CHANNEL);
	PHY_SetRxState(true);

	NWK_OpenEndpoint(10, appDataInd); //APP_ENDPOINT

	HAL_BoardInit();
	
	/* Define Initial values */
	config.reg_state = UNREGISTRED;
	config.mode = ONE_SHOT;
}

/*************************************************************************//**
*****************************************************************************/
static void APP_TaskHandler(void)
{
	switch (appState)
	{
		case APP_STATE_INITIAL:
		{
			appInit();
			appState = APP_STATE_IDLE;
		} break;
		case APP_STATE_IDLE:
		break;
		default:
		break;
	}
}

/*************************************************************************//**
*****************************************************************************/
int main(void)
{
	SYS_Init();
	HAL_UartInit(38400);
	HAL_UartWriteByte('Hello');
	
	while (1)
	{
		SYS_TaskHandler();
		HAL_UartTaskHandler();
		APP_TaskHandler();
	}
}
