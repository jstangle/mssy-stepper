

#ifndef MAIN_H_
#define MAIN_H_

/*************PERIFERIE*******************************/
#ifdef HAL_ATMEGA256RFR2
#define LED0 PB4
#define LED1 PB5
#define LED2 PB6
#define LED3 PE3
#define BUT1 PE5
#define BUT2 PF0
#define BUT3 PF1
#endif
/*****************************************************/
/*************MAKRA***********************************/
#define sbi(var, mask)  ((var) |= (uint8_t)(1 << mask))
#define cbi(var, mask)  ((var) &= (uint8_t)~(1 << mask))
#define tbi(var,mask)	(var & (1 << mask) )
#define xbi(var,mask)	((var)^=(uint8_t)(1 << mask))
/************Stepper**********************************/
#define SETBIT(VAR, BIT )(VAR |= ( 1 << BIT ) )
#define CLEARBIT(VAR,BIT)(VAR &= ~(1 << BIT))
#define ISSET(VAR, BIT)(VAR & ( 1 << BIT ))
#define CHANGEBIT(VAR,BIT)(VAR^=(1 << BIT))

#define Aon SETBIT(PORTE,2);
#define Aoff CLEARBIT(PORTE, 2);
#define Bon SETBIT(PORTE,3);
#define Boff CLEARBIT(PORTE, 3);
#define Con SETBIT(PORTE,5);
#define Coff CLEARBIT(PORTE, 5);
#define Don SETBIT(PORTE,6);
#define Doff CLEARBIT(PORTE, 6);
#define LED0on SETBIT(DDRB,4);
#define LED0off CLEARBIT(DDRB, 4);
#define LED0change CHANGEBIT(DDRB,4);
/*****************************************************/
#ifdef HAL_ATMEGA256RFR2
#define LED0ON (cbi(PORTB,LED0))
#define LED0OFF (sbi(PORTB,LED0))
#define LED0SW (xbi(PORTB,LED0))
#define LED1ON (cbi(PORTB,LED1))
#define LED1OFF (sbi(PORTB,LED1))
#define LED1SW (xbi(PORTB,LED1))
#define LED2ON (cbi(PORTB,LED2))
#define LED2OFF (sbi(PORTB,LED2))
#define LED2SW (xbi(PORTB,LED2))
#define LED3ON (cbi(PORTE,LED3))
#define LED3OFF (sbi(PORTE,LED3))
#define LED3SW (xbi(PORTE,LED3))
#endif

#endif /* MAIN_H_ */